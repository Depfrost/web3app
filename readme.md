# Krypt - Web 3.0 Blockchain Application
![Krypt](https://i.ibb.co/DVF4tNW/image.png)

## Introduction
This is a code repository based on and inspired by JavaScript Mastery's video tutorial.
Using Web 3.0 methodologies, Solidity and Metamask I learned how to build a my first Web 3.0 Application from start to finish.

To make it work you will have to provide:
 - GIPHY_API : A giphy api key to the client via an .env file.
 - PRIVATE_KEY : The private key used to deploy the smart contract.
 - URL : A link to alchemy api to help us deploy smart contracts.